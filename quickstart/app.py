from peewee import *
from datetime import date

db = SqliteDatabase('people.db')

class Person(Model):
    name = CharField()
    birthday = DateField()

    class Meta:
        database = db # This model uses the "people.db" database.
        table_name = 'tab_person' # can rename the table

class Pet(Model):
    owner = ForeignKeyField(Person, backref='pets')
    name = CharField()
    animal_type = CharField()

    class Meta:
        database = db # this model uses the "people.db" database

def display_table(table, table_name=None):

    print("#### Display table ####")

    if table_name:
        print(table_name,':')

    for row in table.select():
        for k,v in row.__data__.items():
            print(k,"=",v,end='\t')
        print()

    print("#######################")

if __name__ == "__main__":
    
    db.connect()
   
    # create tables
    db.create_tables([Person, Pet])

    # create one person entry
    uncle_bob = Person(name='Bob', birthday=date(1960, 1, 15))
    uncle_bob.save() # bob is now stored in the database

    # another way to create
    grandma = Person.create(name='Grandma', birthday=date(1935, 3, 1))
    herb = Person.create(name='Herb', birthday=date(1950, 5, 5))


    b_kitty = Pet.create(owner=uncle_bob, name='Kitty', animal_type='cat')
    herb_fido = Pet.create(owner=herb, name='Fido', animal_type='dog')
    herb_mittens = Pet.create(owner=herb, name='Mittens', animal_type='cat')
    herb_mittens_jr = Pet.create(owner=herb, name='Mittens Jr', animal_type='cat')

    ret = herb_mittens.delete_instance() # he had a great life
    # Returns: 1, the number of rows removed
    print("herb mittens is gone :",ret)

    print("Bob adopts Fido:")
    herb_fido.owner = uncle_bob
    herb_fido.save()

    display_table(Person, "Person")
    display_table(Pet, "Pet")

    print("Let’s list all the cats and their owner’s name:")
    print("wrong way to do it:")
    query = Pet.select().where(Pet.animal_type == 'cat')
    for pet in query:
        print("pet:",pet.name,"owner:",pet.owner.name)
        # There is a big problem with the previous query:
        # because we are accessing pet.owner.name and we did not select this relation in our original query,
        # peewee will have to perform an additional query to retrieve the pet’s owner.
        # This behavior is referred to as N+1 and it should generally be avoided.

    # better way to do it:
    query = (Pet.select(Pet, Person).join(Person).where(Pet.animal_type == 'cat'))

    print("better way to do it")
    for pet in query:
        print("pet:",pet.name,"owner:",pet.owner.name)


    print("select pets owned by Bob using is name:")
    for pet in Pet.select().join(Person).where(Person.name == 'Bob'):
        print(pet.name)

    print("same but directly using uncle_bob object:")
    for pet in Pet.select().where(Pet.owner == uncle_bob):
        print(pet.name)

    print("#sorting")
    print("alphabetical order")
    for pet in Pet.select().where(Pet.owner == uncle_bob).order_by(Pet.name):
        print(pet.name)

    print("by birthday")
    for person in Person.select().order_by(Person.birthday.desc()):
        print(person.name, person.birthday)
