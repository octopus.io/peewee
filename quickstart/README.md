# peewee quickstart

This code follow instruction from
http://docs.peewee-orm.com/en/latest/peewee/quickstart.html

# sqlite

To display tables present in people.db:
`sqlite3 people.db .tables`

To get structure of on table:
`sqlite3 people.db .schema`

To display entrys:
`sqlite3 people.db "select * from person"`
